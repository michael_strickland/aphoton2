/** 
*
*   Vector4D.h
*	
*   Copyright (C) 2017 Radoslaw Ryblewski and Michael Strickland
*
*   This file is part of aHydro3p1.
*
*   aHydro3p1 is free software: you can redistribute it and/or modify
*   it under the terms of the GNU General Public License as published by
*   the Free Software Foundation, either version 3 of the License, or
*   (at your option) any later version.
*
*   aHydro3p1 is distributed in the hope that it will be useful,
*   but WITHOUT ANY WARRANTY; without even the implied warranty of
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*   GNU General Public License for more details.
*
*   You should have received a copy of the GNU General Public License
*   along with aHydro3p1.  If not, see <http://www.gnu.org/licenses/>.
*    
*/

#ifndef _VECTOR4D_H_
#define _VECTOR4D_H_

#include <complex>
using namespace std;

class Vector4D
{
  public:

    // operator () for reading/writing element of Vector

    inline double& operator()(unsigned int n,unsigned int i, unsigned int j, unsigned int k);

    // it may be required to define operator= here

// CLASS FUNCTIONS
    Vector4D();
    Vector4D(string aName,
	     double aTmin, double aTmax, int aTpts,
	     double aXmin, double aXmax, int aXpts,
	     double aYmin, double aYmax, int aYpts,
	     double aZmin, double aZmax, int aZpts);

    Vector4D(const Vector4D& aVector);
    ~Vector4D();

    string GetName() const;
    double     GetTMin() const;
    double     GetTMax() const;
    int         GetTPts() const;
    double     GetXMin() const;
    double     GetXMax() const;
    int         GetXPts() const;
    double     GetYMin() const;
    double     GetYMax() const;
    int         GetYPts() const;
    double     GetZMin() const;
    double     GetZMax() const;
    int         GetZPts() const;

    double	Interpolate(double aT, double aX, double aY, double aZ);

  private:

    double	Interpolate1D(double aX);
    double	Interpolate2D(double aX, double aY);
    double	Interpolate3D(double aX, double aY, double aZ);
    double	Interpolate4D(double aT, double aX, double aY, double aZ);

//    inline int	  InitDerivative(int aIdx, doubleaAMin, doubleaAMax, int aAPts);
//    inline doubleDerivative(doubleaFin, doubleaFi, doubleaFip);

    string	mVecName;
    double****	mVec;

    double	mTmin, mTmax;
    int		mTpts;
    double	mDn;

    double	mXmin, mXmax;
    int		mXpts;
    double	mDi;

    double	mYmin, mYmax;
    int		mYpts;
    double	mDj;

    double	mZmin, mZmax;
    int		mZpts;
    double	mDk;

};

// OPERATORS

inline double& Vector4D::operator()(const unsigned int n, const unsigned int i, const unsigned int j, const unsigned int k)
{
  return mVec[n][i][j][k];
}

#endif //_VECTOR4D_H_
