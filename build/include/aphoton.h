#ifndef __softpart_h__
#define __softpart_h__

// params loaded from params.txt
extern double ALPHAS, CHARGEFACTOR, XI;
extern int USESIGMAINT,TESTSIGMA;

// computed params
extern double G, MQHAT;

// debug level 0 = off, >0 = on
#define DEBUG_LEVEL 0

// fine structure constant
#define ALPHAEM 7.29735257e-3

// column width for output
#define DWIDTH 20

// hbar*c
#define HBARC  0.197326938

void totalRate(double qhat, double tq, double pstarhat, double xi, double *res, double *err);
void print_line();
void printTime();

#endif /* __softpart_h__ */
