#ifndef __selfenergies_h__
#define __selfenergies_h__

#include "Vector4D.h"

inline double sign(double z) {
    return z != 0 ? fabs(z)/z : 1;
}

struct integralParams {
    double z, tk, fk, xi1, xi2;
};

struct integralParams1 {
    double z, tk, fk, xi1, xi2, x;
};

#define SMALL 1.0e-8
#define BIG 1.0e8
#define NPTS 100
#define ERRGOAL 1.0e-6
#define INT_METHOD GSL_INTEG_GAUSS21

// interpolation dimensions
#define NFITPTS 101
#define NCOEFFS    20
#define NT  41
#define NF  41
#define NXI1    41
#define NXI2    41
// range of ln(1+xi)
#define L1PXI_MIN   -5
#define L1PXI_MAX   5

// some gsl related functions for convenience
#define COEFF(i) (gsl_vector_get(c,(i)))
#define COV(i,j) (gsl_matrix_get(cov,(i),(j)))

// direct numerical integration
double ReSigma0(double, double, double, double, double);
double ReSigma1(double, double, double, double, double);
double ReSigma2(double, double, double, double, double);
double ReSigma3(double, double, double, double, double);

double ImSigma0(double, double, double, double, double);
double ImSigma1(double, double, double, double, double);
double ImSigma2(double, double, double, double, double);
double ImSigma3(double, double, double, double, double);

// babak's trick
gsl_vector* fitImSigma(double (*fp)(double,double,double,double,double), double, double, double, double, int);

double ReSigma(gsl_vector* c, double, int);
double ImSigma(gsl_vector* c, double, int);

void initializeCoeffVector(Vector4D**);
void freeCoeffVector(Vector4D**);
void computeCoeffVector(Vector4D**, double (*fp)(double,double,double,double,double), int);

double ReSigma_Int(Vector4D**, double, double, double, double, double, int);
double ImSigma_Int(Vector4D**, double, double, double, double, double, int);

void saveCoeffVector(string, Vector4D**);
void readCoeffVector(string, Vector4D**);
void loadCoeffVector(Vector4D**, double (*ifp)(double,double,double,double,double), int, int);
void testSigma(Vector4D**, double (*fp)(double,double,double,double,double), double (*rfp)(double,double,double,double,double), int, int);

#endif /* __selfenergies_h__ */
