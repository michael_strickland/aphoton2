/** 
 *
 *   Vector4D.cxx
 *
 *   Copyright (C) 2017 Radoslaw Ryblewski and Michael Strickland
 *
 *   This file is part of aHydro3p1.
 *
 *   aHydro3p1 is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   aHydro3p1 is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with aHydro3p1.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>


#include "Vector4D.h"

// CLASS FUNCTIONS

Vector4D::Vector4D()
: mVecName(""), mVec(0),
mTmin(0.0), mTmax(1.0), mTpts(1),
mXmin(0.0), mXmax(1.0), mXpts(1),
mYmin(0.0), mYmax(1.0), mYpts(1),
mZmin(0.0), mZmax(1.0), mZpts(1)
{
    mVec		= new double*** [1];
    mVec[0]	= new double**  [1];
    mVec[0][0]	= new double*   [1];
    mVec[0][0][0]	= new double   [1];
    mVec[0][0][0][0]	= 0.0;
}

Vector4D::Vector4D(string aName,
                   double aTmin, double aTmax, int aTpts,
                   double aXmin, double aXmax, int aXpts,
                   double aYmin, double aYmax, int aYpts,
                   double aZmin, double aZmax, int aZpts)
: mVecName(aName),
mTmin(aTmin), mTmax(aTmax), mTpts(aTpts),
mXmin(aXmin), mXmax(aXmax), mXpts(aXpts),
mYmin(aYmin), mYmax(aYmax), mYpts(aYpts),
mZmin(aZmin), mZmax(aZmax), mZpts(aZpts)
{
    if(mTpts <= 0) mTpts = 1;
    if(mXpts <= 0) mXpts = 1;
    if(mYpts <= 0) mYpts = 1;
    if(mZpts <= 0) mZpts = 1;
    
    mDn = (mTpts - 1) / (mTmax - mTmin);
    mDi = (mXpts - 1) / (mXmax - mXmin);
    mDj = (mYpts - 1) / (mYmax - mYmin);
    mDk = (mZpts - 1) / (mZmax - mZmin);
    
    mVec = new double*** [mTpts];
    for (int n=0; n<mTpts; n++) {
        mVec[n] = new double** [mXpts];
        for (int i=0; i<mXpts; i++) {
            mVec[n][i] = new double* [mYpts];
            for (int j=0; j<mYpts; j++) {
                mVec[n][i][j] = new double [mZpts];
                for (int k=0; k<mZpts; k++)
                    mVec[n][i][j][k] = 0.0;
            }
        }
    }
    
}

Vector4D::Vector4D(const Vector4D& aVector)
{
    mTmin = aVector.mTmin; mTmax = aVector.mTmax; mTpts = aVector.mTpts; mDn = aVector.mDn;
    mXmin = aVector.mXmin; mXmax = aVector.mXmax; mXpts = aVector.mXpts; mDi = aVector.mDi;
    mYmin = aVector.mYmin; mYmax = aVector.mYmax; mYpts = aVector.mYpts; mDj = aVector.mDj;
    mZmin = aVector.mZmin; mZmax = aVector.mZmax; mZpts = aVector.mZpts; mDk = aVector.mDk;
    
    mVecName = "";
    mVec = new double*** [mTpts];
    for (int n=0; n<mTpts; n++) {
        mVec[n] = new double** [mXpts];
        for (int i=0; i<mXpts; i++) {
            mVec[n][i] = new double* [mYpts];
            for (int j=0; j<mYpts; j++) {
                mVec[n][i][j] = new double [mZpts];
                for (int k=0; k<mZpts; k++)
                    mVec[n][i][j][k] = 0.0;
            }
        }
    }
}

Vector4D::~Vector4D()
{
    if(mVec) {
        
        // for 4D
        for (int n=0; n<mTpts; n++) {
            for (int i=0; i<mXpts; i++) {
                for (int j=0; j<mYpts; j++){
                    delete[] mVec[n][i][j];
                }
                delete[] mVec[n][i];
            }
            delete[] mVec[n];
        }
        delete[] mVec;
        
    }
}

string  	Vector4D::GetName() const	{ return mVecName.c_str(); }
double		Vector4D::GetTMin() const	{ return mTmin; }
double		Vector4D::GetTMax() const	{ return mTmax; }
int		    Vector4D::GetTPts() const	{ return mTpts; }
double		Vector4D::GetXMin() const	{ return mXmin; }
double		Vector4D::GetXMax() const	{ return mXmax; }
int		    Vector4D::GetXPts() const	{ return mXpts; }
double		Vector4D::GetYMin() const	{ return mYmin; }
double		Vector4D::GetYMax() const	{ return mYmax; }
int		    Vector4D::GetYPts() const	{ return mYpts; }
double		Vector4D::GetZMin() const	{ return mZmin; }
double		Vector4D::GetZMax() const	{ return mZmax; }
int		    Vector4D::GetZPts() const	{ return mZpts; }

double Vector4D::Interpolate(double aT, double aX, double aY, double aZ)
{
    if(mZpts>1)
        return Interpolate4D(aT, aX, aY, aZ);
    else if (mYpts>1)
        return Interpolate3D(aT, aX, aY);
    else if (mXpts>1)
        return Interpolate2D(aT, aX);
    else if (mTpts>1)
        return Interpolate1D(aT);
    else {
        return mVec[0][0][0][0];
    }
}

double Vector4D::Interpolate1D(double aT)
{
    int    n, i, j, k;
    double tn;
    
    tn = (aT - mTmin) * mDn;	n = (int) tn;	if(n+1 > mTpts-1) n--;	tn -= n;
    i  = 0;
    j  = 0;
    k  = 0;
    return
    mVec[n  ][i ][j  ][k  ] * (1-tn) + mVec[n+1][i][j  ][k  ] * tn; //Linear Interpolation, weighted average:f(x) = (1-x)*f(0) + x*f(1)
}

double Vector4D::Interpolate2D(double aT, double aX)
{
    int    n, i, j, k;
    double tn,ti;
    
    tn = (aT - mTmin) * mDn;	n = (int) tn;	if(n+1 > mTpts-1) n--;	tn -= n;
    ti = (aX - mXmin) * mDi;	i = (int) ti;	if(i+1 > mXpts-1) i--;	ti -= i;
    j  = 0;
    k  = 0;
    return
    (mVec[n ][i  ][j  ][k  ] * (1-tn) + mVec[n+1][i  ][j  ][k  ] * tn) * (1-ti) +
    (mVec[n ][i+1][j  ][k  ] * (1-tn) + mVec[n+1][i+1][j  ][k  ] * tn) *    ti;
}

double Vector4D::Interpolate3D(double aT, double aX, double aY)
{
    int    n, i, j, k;
    double tn,ti,tj;
    
    tn = (aT - mTmin) * mDn;	n = (int) tn;	if(n+1 > mTpts-1) n--;	tn -= n;
    ti = (aX - mXmin) * mDi;	i = (int) ti;	if(i+1 > mXpts-1) i--;	ti -= i;
    tj = (aY - mYmin) * mDj;	j = (int) tj;	if(j+1 > mYpts-1) j--;	tj -= j;
    k  = 0;
    return
    (
     (mVec[n  ][i  ][j  ][k] * (1-tn) + mVec[n+1][i  ][j  ][k] * tn) * (1-ti) +
     (mVec[n  ][i+1][j  ][k] * (1-tn) + mVec[n+1][i+1][j  ][k] * tn) *    ti
     ) * (1-tj) + (
                   (mVec[n  ][i  ][j+1][k] * (1-tn) + mVec[n+1][i  ][j+1][k] * tn) * (1-ti) +
                   (mVec[n  ][i+1][j+1][k] * (1-tn) + mVec[n+1][i+1][j+1][k] * tn) *    ti
                   ) *    tj;
}

double Vector4D::Interpolate4D(double aT, double aX, double aY, double aZ)
{
    int    n, i, j, k;
    double tn,ti,tj,tk;
    
    tn = (aT - mTmin) * mDn;	n = (int) tn;	if(n+1 > mTpts-1) n--;	tn -= n;
    ti = (aX - mXmin) * mDi;	i = (int) ti;	if(i+1 > mXpts-1) i--;	ti -= i;
    tj = (aY - mYmin) * mDj;	j = (int) tj;	if(j+1 > mYpts-1) j--;	tj -= j;
    tk = (aZ - mZmin) * mDk;	k = (int) tk;	if(k+1 > mZpts-1) k--;	tk -= k;

    /*
    cout << "debug -  t, x, y, z - " << aT << "\t";
    cout << aX << "\t";
    cout << aY << "\t";
    cout << aZ << endl;
    cout << "debug -  n, i, j, k - " << n << "\t";
    cout << i << "\t";
    cout << j << "\t";
    cout << k << endl;
    cout << "debug -  tn, ti, tj, tk - " << tn << "\t";
    cout << ti << "\t";
    cout << tj << "\t";
    cout << tk << endl;
     */
    
    return
    (
     (
      (mVec[n  ][i  ][j  ][k] * (1-tn) + mVec[n+1][i  ][j  ][k] * tn) * (1-ti) +
      (mVec[n  ][i+1][j  ][k] * (1-tn) + mVec[n+1][i+1][j  ][k] * tn) *    ti
      ) * (1-tj)
     +
     (
      (mVec[n  ][i  ][j+1][k] * (1-tn) + mVec[n+1][i  ][j+1][k] * tn) * (1-ti) +
      (mVec[n  ][i+1][j+1][k] * (1-tn) + mVec[n+1][i+1][j+1][k] * tn) *    ti
      ) *    tj
     ) * (1-tk)
    +
    (
     (
      (mVec[n  ][i  ][j  ][k+1] * (1-tn) + mVec[n+1][i  ][j  ][k+1] * tn) * (1-ti) +
      (mVec[n  ][i+1][j  ][k+1] * (1-tn) + mVec[n+1][i+1][j  ][k+1] * tn) *    ti
      ) * (1-tj)
     +
     (
      (mVec[n  ][i  ][j+1][k+1] * (1-tn) + mVec[n+1][i  ][j+1][k+1] * tn) * (1-ti) +
      (mVec[n  ][i+1][j+1][k+1] * (1-tn) + mVec[n+1][i+1][j+1][k+1] * tn) *    ti
      ) *    tj
     ) * tk;
    
}
