#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <vector>

using namespace std;

#include "aphoton.h"
#include "paramreader.h"
#include "selfenergies.h"

// default values; overridden by params.txt file
double ALPHAS=0.32, CHARGEFACTOR=0.555556,XI=0;
int USESIGMAINT=0,TESTSIGMA=4;

// variables computed in paramreader.cpp processParameters based on params.txt values
double G=1, MQHAT=1;

void print_line() {
    for (int i=0;i<3*DWIDTH;i++) cout << "-"; cout << endl;
}

void printTime() {
    char buff[100];
    time_t rawtime;
    time(&rawtime);
    strftime(buff, 100, "%Y-%m-%d %H:%M:%S", localtime(&rawtime));
    cout << " " << buff << endl;
}

// main routine
int main(int argc, char** argv)
{
    // format for output
    cout << std::scientific;
    
    print_line();
    printTime(); // print time
    print_line();
    
    // read parameters from file and command line
    readParametersFromFile("input/params.txt",1);
    if (argc>1) {
        print_line();
        cout << "Parameters from commandline" << endl;
        print_line();
        readParametersFromCommandLine(argc,argv,1);
    }
    // perform any processing of parameters necessary
    processParameters();
    
    print_line();
    
    // Sigma0
    if (TESTSIGMA==0||TESTSIGMA==4) {
        Vector4D *coeffVector0[NCOEFFS];
        initializeCoeffVector(coeffVector0);
        loadCoeffVector(coeffVector0,&ImSigma0,0,0);
        testSigma(coeffVector0,&ReSigma0,&ImSigma0,0,0);
        freeCoeffVector(coeffVector0);
    }
    
    // Sigma1
    if (TESTSIGMA==1||TESTSIGMA==4) {
        Vector4D *coeffVector1[NCOEFFS];
        initializeCoeffVector(coeffVector1);
        loadCoeffVector(coeffVector1,&ImSigma1,1,1);
        testSigma(coeffVector1,&ReSigma1,&ImSigma1,1,1);
        freeCoeffVector(coeffVector1);
    }
    
    // Sigma2
    if (TESTSIGMA==2||TESTSIGMA==4) {
        Vector4D *coeffVector2[NCOEFFS];
        initializeCoeffVector(coeffVector2);
        loadCoeffVector(coeffVector2,&ImSigma2,2,1);
        testSigma(coeffVector2,&ReSigma2,&ImSigma2,2,1);
        freeCoeffVector(coeffVector2);
    }
    
    // Sigma3
    if (TESTSIGMA==3||TESTSIGMA==4) {
        Vector4D *coeffVector3[NCOEFFS];
        initializeCoeffVector(coeffVector3);
        loadCoeffVector(coeffVector3,&ImSigma3,3,1);
        testSigma(coeffVector3,&ReSigma3,&ImSigma3,3,1);
        freeCoeffVector(coeffVector3);
    }
    
    print_line();
    printTime(); // print final time
    print_line();
    cout << " DONE" << endl;
    print_line();
    
    return 0;
}
