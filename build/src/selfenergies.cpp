#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_multifit.h>
#include <gsl/gsl_sf_gamma.h>
#include <gsl/gsl_sf_hyperg.h>

using namespace std;

#include "selfenergies.h"
#include "aphoton.h"

// derived variables; used in interpolations
double DT = M_PI/(NT-1);
double DF = 2*M_PI/(NF-1);
double DLXI1 = ((double)(L1PXI_MAX - L1PXI_MIN))/(NXI1-1);
double DLXI2 = ((double)(L1PXI_MAX - L1PXI_MIN))/(NXI2-1);

/*
 *
 *  Re[Sigma]
 *
 */

double g0(double x, double f, double tk, double fk, double xi1, double xi2) {
    double tmp1 = cos(tk)*x - sin(tk)*sqrt(1-x*x)*sin(f);
    double tmp2 = sin(tk)*cos(fk)*x + cos(tk)*cos(fk)*sqrt(1-x*x)*sin(f)+sin(fk)*sqrt(1-x*x)*cos(f);
    return 1./(1+xi1*tmp1*tmp1 + xi2*tmp2*tmp2);
}

double gcos(double x, double f, double tk, double fk, double xi1, double xi2) {
    double tmp1 = cos(tk)*x - sin(tk)*sqrt(1-x*x)*sin(f);
    double tmp2 = sin(tk)*cos(fk)*x + cos(tk)*cos(fk)*sqrt(1-x*x)*sin(f)+sin(fk)*sqrt(1-x*x)*cos(f);
    return cos(f)/(1+xi1*tmp1*tmp1 + xi2*tmp2*tmp2);
}

double gsin(double x, double f, double tk, double fk, double xi1, double xi2) {
    double tmp1 = cos(tk)*x - sin(tk)*sqrt(1-x*x)*sin(f);
    double tmp2 = sin(tk)*cos(fk)*x + cos(tk)*cos(fk)*sqrt(1-x*x)*sin(f)+sin(fk)*sqrt(1-x*x)*cos(f);
    return sin(f)/(1+xi1*tmp1*tmp1 + xi2*tmp2*tmp2);
}

double ReSigma0_integrand1(double f, void * params) {
    struct integralParams1 *my_params = (struct integralParams1 *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    double x = my_params->x;
    return g0(x,f,tk,fk,xi1,xi2)/(z-x)/(4*M_PI);
}

double ReSigma1_integrand1(double f, void * params) {
    struct integralParams1 *my_params = (struct integralParams1 *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    double x = my_params->x;
    
    //return ((1./(sin(tk)*cos(fk)))*((sin(fk)*sqrt(1-x*x))*gcos(x,f,tk,fk,xi1,xi2)+(cos(tk)*cos(fk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(sin(tk)*cos(fk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
    return (((sin(fk)*sqrt(1-x*x))*gcos(x,f,tk,fk,xi1,xi2)+(cos(tk)*cos(fk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(sin(tk)*cos(fk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
}

double ReSigma2_integrand1(double f, void * params) {
    struct integralParams1 *my_params = (struct integralParams1 *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    double x = my_params->x;
    
    //return ((1./(sin(tk)*sin(fk)))*(-(cos(fk)*sqrt(1-x*x))*gcos(x,f,tk,fk,xi1,xi2)+(cos(tk)*sin(fk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(sin(tk)*sin(fk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
    return ((-(cos(fk)*sqrt(1-x*x))*gcos(x,f,tk,fk,xi1,xi2)+(cos(tk)*sin(fk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(sin(tk)*sin(fk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
    
}

double ReSigma3_integrand1(double f, void * params) {
    struct integralParams1 *my_params = (struct integralParams1 *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    double x = my_params->x;
    
    //return ((1./(cos(tk)))*(-(sin(tk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(cos(tk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
    return ((-(sin(tk)*sqrt(1-x*x))*gsin(x,f,tk,fk,xi1,xi2)+(cos(tk)*x)*g0(x,f,tk,fk,xi1,xi2)))/(z-x)/(4*M_PI);
    
}

double ReSigma0_intf(double x, void * params) {
    
    double result=0, error=0;
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    
    struct integralParams1 my_params1 = { z, tk, fk, xi1, xi2, x };
    
    gsl_function F;
    F.function = &ReSigma0_integrand1;
    F.params = &my_params1;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ReSigma1_intf(double x, void * params) {
    
    double result=0, error=0;
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    
    struct integralParams1 my_params1 = { z, tk, fk, xi1, xi2, x };
    
    gsl_function F;
    F.function = &ReSigma1_integrand1;
    F.params = &my_params1;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ReSigma2_intf(double x, void * params) {
    
    double result=0, error=0;
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    
    struct integralParams1 my_params1 = { z, tk, fk, xi1, xi2, x };
    
    gsl_function F;
    F.function = &ReSigma2_integrand1;
    F.params = &my_params1;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ReSigma3_intf(double x, void * params) {
    
    double result=0, error=0;
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    
    struct integralParams1 my_params1 = { z, tk, fk, xi1, xi2, x };
    
    gsl_function F;
    F.function = &ReSigma3_integrand1;
    F.params = &my_params1;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ReSigma0(double z, double tk, double fk, double xi1, double xi2) {
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ReSigma0_intf;
    F.params = &params;
    
    if (fabs(z) >1) {
        gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
        gsl_integration_qag(&F, -1, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
        gsl_integration_workspace_free(w);
        
    } else {
        double res1=0, res2=0, err1=0, err2=0;
        double lim1 = z - SMALL;
        double lim2 = z + SMALL;
        if (lim1 < -1) {
            res1 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, -1, lim1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res1, &err1);
            gsl_integration_workspace_free(w);
        }
        if (lim2>1) {
            res2 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, lim2, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res2, &err2);
            gsl_integration_workspace_free(w);
        }
        result = res1 + res2;
    }
    
    return result;
}

double ReSigma1(double z, double tk, double fk, double xi1, double xi2) {
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ReSigma1_intf;
    F.params = &params;
    
    if (fabs(z) >1) {
        gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
        gsl_integration_qag(&F, -1, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
        gsl_integration_workspace_free(w);
        
    } else {
        double res1=0, res2=0, err1=0, err2=0;
        double lim1 = z - SMALL;
        double lim2 = z + SMALL;
        if (lim1 < -1) {
            res1 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, -1, lim1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res1, &err1);
            gsl_integration_workspace_free(w);
        }
        if (lim2>1) {
            res2 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, lim2, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res2, &err2);
            gsl_integration_workspace_free(w);
        }
        result = res1 + res2;
    }
    
    return result;
}

double ReSigma2(double z, double tk, double fk, double xi1, double xi2) {
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ReSigma2_intf;
    F.params = &params;
    
    if (fabs(z) >1) {
        gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
        gsl_integration_qag(&F, -1, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
        gsl_integration_workspace_free(w);
        
    } else {
        double res1=0, res2=0, err1=0, err2=0;
        double lim1 = z - SMALL;
        double lim2 = z + SMALL;
        if (lim1 < -1) {
            res1 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, -1, lim1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res1, &err1);
            gsl_integration_workspace_free(w);
        }
        if (lim2>1) {
            res2 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, lim2, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res2, &err2);
            gsl_integration_workspace_free(w);
        }
        result = res1 + res2;
    }
    
    return result;
}

double ReSigma3(double z, double tk, double fk, double xi1, double xi2) {
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ReSigma3_intf;
    F.params = &params;
    
    if (fabs(z) >1) {
        gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
        gsl_integration_qag(&F, -1, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
        gsl_integration_workspace_free(w);
        
    } else {
        double res1=0, res2=0, err1=0, err2=0;
        double lim1 = z - SMALL;
        double lim2 = z + SMALL;
        if (lim1 < -1) {
            res1 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, -1, lim1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res1, &err1);
            gsl_integration_workspace_free(w);
        }
        if (lim2>1) {
            res2 = 0;
        } else {
            gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
            gsl_integration_qag(&F, lim2, 1, ERRGOAL, 0, NPTS, INT_METHOD, w, &res2, &err2);
            gsl_integration_workspace_free(w);
        }
        result = res1 + res2;
    }
    
    return result;
}


/*
 *
 *  Im[Sigma]
 *
 */

double ImSigma0_integrand(double f, void * params) {
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    return -0.25*g0(z,f,tk,fk,xi1,xi2);
}

double ImSigma1_integrand(double f, void * params) {
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    //return -0.25*(1./(sin(tk)*cos(fk)))*( (sin(fk)*sqrt(1-z*z))*gcos(z,f,tk,fk,xi1,xi2)+ (cos(tk)*cos(fk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (sin(tk)*cos(fk)*z)*g0(z,f,tk,fk,xi1,xi2));
    return -0.25*( (sin(fk)*sqrt(1-z*z))*gcos(z,f,tk,fk,xi1,xi2)+ (cos(tk)*cos(fk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (sin(tk)*cos(fk)*z)*g0(z,f,tk,fk,xi1,xi2));
}

double ImSigma2_integrand(double f, void * params) {
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    //return -0.25*(1./(sin(tk)*sin(fk)))*( -(cos(fk)*sqrt(1-z*z))*gcos(z,f,tk,fk,xi1,xi2)+ (cos(tk)*sin(fk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (sin(tk)*sin(fk)*z)*g0(z,f,tk,fk,xi1,xi2));
    return -0.25*( -(cos(fk)*sqrt(1-z*z))*gcos(z,f,tk,fk,xi1,xi2)+ (cos(tk)*sin(fk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (sin(tk)*sin(fk)*z)*g0(z,f,tk,fk,xi1,xi2));
}

double ImSigma3_integrand(double f, void * params) {
    struct integralParams *my_params = (struct integralParams *) params;
    double z = my_params->z;
    double tk = my_params->tk;
    double fk = my_params->fk;
    double xi1 = my_params->xi1;
    double xi2 = my_params->xi2;
    //return -0.25*(1./(cos(tk)))*( -(sin(tk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (cos(tk)*z)*g0(z,f,tk,fk,xi1,xi2));
    return -0.25*( -(sin(tk)*sqrt(1-z*z))*gsin(z,f,tk,fk,xi1,xi2) + (cos(tk)*z)*g0(z,f,tk,fk,xi1,xi2));
}


double ImSigma0(double z, double tk, double fk, double xi1, double xi2) {
    
    if (fabs(z)>1) return 0;
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ImSigma0_integrand;
    F.params = &params;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ImSigma1(double z, double tk, double fk, double xi1, double xi2) {
    
    if (fabs(z)>1) return 0;
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ImSigma1_integrand;
    F.params = &params;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ImSigma2(double z, double tk, double fk, double xi1, double xi2) {
    
    if (fabs(z)>1) return 0;
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ImSigma2_integrand;
    F.params = &params;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

double ImSigma3(double z, double tk, double fk, double xi1, double xi2) {
    
    if (fabs(z)>1) return 0;
    
    double result=0, error=0;
    struct integralParams params = { z, tk, fk, xi1, xi2 };
    
    gsl_function F;
    F.function = &ImSigma3_integrand;
    F.params = &params;
    
    gsl_integration_workspace *w = gsl_integration_workspace_alloc(NPTS);
    gsl_integration_qag(&F, 0, 2*M_PI, ERRGOAL, 0, NPTS, INT_METHOD, w, &result, &error);
    gsl_integration_workspace_free(w);
    
    return result;
    
}

// -------------------------------------------------
//  Routines for implementing the global fit method
// -------------------------------------------------

//
// fits ImSigma; returns a gsl vector of coefficients from the resulting fit
//
gsl_vector* fitImSigma(double (*fp)(double,double,double,double,double), double tk, double fk, double xi1, double xi2, int alpha) {
    
    int nPts=NFITPTS;
    int fitOrder=NCOEFFS;
    
    if (fitOrder > nPts) { cout << "==> Are you crazy?" << endl; exit(-1); }
    
    gsl_matrix *X, *cov;
    gsl_vector *y, *w, *c, *cm;
    double chisq;
    
    X = gsl_matrix_alloc(nPts, fitOrder);
    y = gsl_vector_alloc(nPts);
    w = gsl_vector_alloc(nPts);
    
    c = gsl_vector_alloc(fitOrder);
    cov = gsl_matrix_alloc(fitOrder, fitOrder);
    
    double dz = 2./(nPts-1);
    
    for (int i = 0; i < nPts; i++)
    {
        double z = -1 + i*dz;
        double yi = (*fp)(z,tk,fk,xi1,xi2);
        
        for (int n=0;n<fitOrder;n++) {
            if(alpha==0) gsl_matrix_set(X, i, n, pow(z,2*n));
            else gsl_matrix_set(X, i, n, pow(z,2*n+1));
        }
        gsl_vector_set (y, i, yi);
        gsl_vector_set (w, i, 1.0/(ERRGOAL*ERRGOAL));
    }
    
    gsl_multifit_linear_workspace* work = gsl_multifit_linear_alloc (nPts, fitOrder);
    gsl_multifit_wlinear (X, w, y, c, cov, &chisq, work);
    gsl_multifit_linear_free (work);
    
    gsl_matrix_free(X);
    gsl_vector_free(y);
    gsl_vector_free(w);
    gsl_matrix_free(cov);
    
    return c;
}

//
// Special Function: [ 2F1(1,n+1,n+2,1./z) +/- 2F1(1,n+1,n+2,-1./z) ]/z/(n+1)
//
double sigma(int n, double z) {
    if (fabs(z)==1) { cout << "sigma is not defined at |z|=1!" << endl; exit(-1); }
    double res = 0;
    if (fabs(z)>1.01) {
        int sign = (n%2==0 ? 1 : -1 );
        res = (gsl_sf_hyperg_2F1(1,n+1,n+2,1./z)+sign*gsl_sf_hyperg_2F1(1,n+1,n+2,-1./z))/z/(n+1);
    }
    else {
        int mmax = (n%2==0 ? n-1 : n);
        res = log(fabs((z-1)/(z+1)));
        for (int m=mmax; m>=1; m=m-2) res += 2*pow(z,-m)/m;
        res *= -pow(z,n);
    }
    return res;
}

//
// Returns ReSigma based on fit coeffs
//
double ReSigma(gsl_vector* c, double z, int alpha) {
    double res = 0;
    if (alpha==0) for (int m=0;m<(c->size);m++) res += COEFF(m)*sigma(2*m,z);
    else for (int m=0;m<(c->size);m++) res += COEFF(m)*sigma(2*m+1,z);
    return -res/M_PI;
}

//
// Returns ImSigma based on fit coeffs
//
double ImSigma(gsl_vector* c, double z, int alpha) {
    if (fabs(z)>1) return 0;
    double res = 0;
    if (alpha==0) for (int m=0;m<(c->size);m++) res += COEFF(m)*pow(z,2*m);
    else for (int m=0;m<(c->size);m++) res += COEFF(m)*pow(z,2*m+1);
    return res;
}

//
// Routines for interpolations
//

//
// Setup each element of the coeffVector array to point to a properly allocated Vector4D object
//
void initializeCoeffVector(Vector4D** aCoeffVector)
{
    for (int i=0; i<NCOEFFS; i++) {
        stringstream sstm;
        sstm << "c" << i;
        Vector4D *myVector = new Vector4D(sstm.str(),0,M_PI,NT,0,2*M_PI,NF,L1PXI_MIN,L1PXI_MAX,NXI1,L1PXI_MIN,L1PXI_MAX,NXI2);
        aCoeffVector[i] = myVector;
    }
}

//
// Free memory associated with a coeffVector by freeing the associate Vector4D objects
//
void freeCoeffVector(Vector4D **aCoeffVector)
{
    for (int i=0; i<NCOEFFS; i++) free(aCoeffVector[i]);
}

//
// computes the coeffVector values based on fits to the ImSigma
// all coeffVectors are loaded with Vector4D that can be interpolated
//
void computeCoeffVector(Vector4D **aCoeffVector, double (*fp)(double,double,double,double,double), int alpha)
{
    for (int i=0; i<NT; i++) {
        double tkVal = i*DT;
        cout << " ==> Thetak = "<< tkVal << endl;
        for (int j=0; j<NF; j++)
            for (int k=0; k<NXI1; k++)
                for (int l=0; l<NXI2; l++) {
                    double tkVal = i*DT;
                    double fkVal = j*DF;
                    double xi1Val = exp(L1PXI_MIN + k*DLXI1)-1;
                    double xi2Val = exp(L1PXI_MIN + l*DLXI2)-1;
                    gsl_vector* c;
                    c = fitImSigma(fp,tkVal,fkVal,xi1Val,xi2Val,alpha);
                    for (int m=0; m<c->size; m++) {
                        Vector4D *v4 = aCoeffVector[m];
                        (*v4)(i,j,k,l) = (double) gsl_vector_get(c,m);
                    }
                    gsl_vector_free(c);
                }
    }
}

//
// Interpolates the coefficient array to a point in the volume using linear interpolation and returns the ReSigma value
//
double ReSigma_Int(Vector4D **aCoeffVector, double z, double tk, double fk, double xi1, double xi2, int alpha) {
    gsl_vector *myCoeffs = gsl_vector_alloc(NCOEFFS);
    for (int m=0; m<NCOEFFS; m++) {
        Vector4D *v4 = aCoeffVector[m];
        double res = v4->Interpolate(tk,fk,log(1+xi1),log(1+xi2));
        gsl_vector_set(myCoeffs,m,res);
    }
    double res = ReSigma(myCoeffs,z,alpha);
    gsl_vector_free(myCoeffs);
    return res;
}

//
// Interpolates the coefficient array to a point in the volume using linear interpolation and returns the ImSigma value
//
double ImSigma_Int(Vector4D **aCoeffVector, double z, double tk, double fk, double xi1, double xi2, int alpha) {
    gsl_vector *myCoeffs = gsl_vector_alloc(NCOEFFS);
    for (int m=0; m<NCOEFFS; m++) {
        Vector4D *v4 = aCoeffVector[m];
        double res = v4->Interpolate(tk,fk,log(1+xi1),log(1+xi2));
        gsl_vector_set(myCoeffs,m,res);
    }
    double res = ImSigma(myCoeffs,z,alpha);
    gsl_vector_free(myCoeffs);
    return res;
}

// --------------------------------------------------
//  Routines for loading, reading, and saving coeffs
// --------------------------------------------------

//
// load coeffVector either from disk or computing
//
void loadCoeffVector(Vector4D** aCoeffVector, double (*fp)(double,double,double,double,double), int index, int alpha) {
    initializeCoeffVector(aCoeffVector);
    stringstream sstm;
    if (USESIGMAINT) {
        cout << " Loading Sigma" << index << " from file" << endl;
        print_line();
        sstm << "input/sigma" << index << "_" << NCOEFFS << "_" << NT << "_" << NF << "_" << NXI1 << "_" << NXI2 << ".bin";
        readCoeffVector(sstm.str(),aCoeffVector);
    }
    else {
        cout << " Computing Sigma" << index << " and saving to file" << endl;
        print_line();
        sstm << "output/sigma" << index << "_" << NCOEFFS << "_" << NT << "_" << NF << "_" << NXI1 << "_" << NXI2 << ".bin";
        computeCoeffVector(aCoeffVector,fp,alpha);
        saveCoeffVector(sstm.str(),aCoeffVector);
    }
}

//
// save coeffVector to disk
//
void saveCoeffVector(string name, Vector4D **aCoeffVector)
{
    ofstream myfile;
    myfile.open(name.c_str(), ios::out | ios::binary);
    double tmp;
    cout << " ==> Writing coeffVector to disk" << endl;
    for (int m=0; m<NCOEFFS; m++) {
        Vector4D *v4 = aCoeffVector[m];
        for (int i=0; i<NT; i++)
            for (int j=0; j<NF; j++)
                for (int k=0; k<NXI1; k++)
                    for (int l=0; l<NXI2; l++) {
                        tmp = (*v4)(i,j,k,l);
                        myfile.write(reinterpret_cast<char*>(&tmp),sizeof(double));
                    }
    }
    myfile.close();
}

//
// read coeffVector from disk
//
void readCoeffVector(string name, Vector4D **aCoeffVector)
{
    ifstream myfile;
    myfile.open(name.c_str(), ios::in | ios::binary);
    
    if (!myfile.is_open()) {
        cout << " !!! Unable to open coeffVector data file (" << name.c_str() << "). Exiting." << endl;
        exit(-1);
    }
    
    double tmp;
    cout << " ==> Reading coeffVector from disk" << endl;
    for (int m=0; m<NCOEFFS; m++) {
        Vector4D *v4 = aCoeffVector[m];
        for (int i=0; i<NT; i++)
            for (int j=0; j<NF; j++)
                for (int k=0; k<NXI1; k++)
                    for (int l=0; l<NXI2; l++) {
                        myfile.read(reinterpret_cast<char*>(&tmp),sizeof(double));
                        (*v4)(i,j,k,l) = tmp;
                    }
    }
    myfile.close();
}

// ------------------
//  Testing routines
// ------------------

//
// generate some test output
//
void testSigma(Vector4D** aCoeffVector,
               double (*rfp)(double,double,double,double,double),
               double (*ifp)(double,double,double,double,double),
               int index,
               int alpha)
{
    
    double tk_test = 2;
    double fk_test = 3;
    double xi1_test = 4;
    double xi2_test = 5;
    
    stringstream sstm1, sstm2;
    sstm1 << "output/output" << index << ".dat";
    sstm2 << "output/output" << index << "-2.dat";
    
    // open files to put the data in first
    fstream file_out,file_out2;
    file_out.open(sstm1.str().c_str(), ios::out);
    file_out2.open(sstm2.str().c_str(), ios::out);
    
    // format for output
    file_out << std::scientific;
    
    int npts = 200;
    double dz = 4./(npts-1);
    
    print_line();
    printTime(); // print time
    print_line();
    
    cout << " Loading test points using direct integration..." << endl;
    for (int i=0;i<npts;i++) {
        double z = -2 + i*dz;
        file_out << z << "\t" << (*rfp)(z,tk_test,fk_test,xi1_test,xi2_test) << "\t" << (*ifp)(z,tk_test,fk_test,xi1_test,xi2_test) << endl;
    }
    
    print_line();
    printTime(); // print time
    print_line();
    
    cout << " Loading test points using coefficient interpolation..." << endl;
    for (int i=0;i<npts;i++) {
        double z = -2 + i*dz;
        file_out2 << z << "\t" << ReSigma_Int(aCoeffVector,z,tk_test,fk_test,xi1_test,xi2_test,alpha) << "\t" << ImSigma_Int(aCoeffVector,z,tk_test,fk_test,xi1_test,xi2_test,alpha) << endl;
    }
    
    print_line();
    
    file_out.close();
    file_out2.close();
}
