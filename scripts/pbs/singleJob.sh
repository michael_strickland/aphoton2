#PBS -N aPhoton
#PBS -l walltime=50:00:00
#PBS -l nodes=1:ppn=1
#PBS -j oe
#PBS -l mem=2G
cd $PBS_O_WORKDIR
./aphoton > log.out
