#!/bin/bash

############################################################################################################
# Loop over log xi and calls calculates the photon rate
############################################################################################################

############################################################################################################
#SBATCH --job-name=aphoton
#SBATCH --nodes=3
#SBATCH --ntasks=24
############################################################################################################

# min and max log10(1+xi)
lximin=-1.0
lximax=2.5
# log10(1+xi) steps
lxisteps=23  # counter runs from 0 -> lxisteps

# compute deltap
deltalxi=$(echo "scale=6; ($lximax-($lximin))/$lxisteps" | bc)

############################################################################################################

function printLine {
echo "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
}

printLine
echo "Initializing variables"
printLine
echo "==> initial log10(1+xi) = " $lximin
echo "==> final log10(1+xi) = " $lximax
echo "==> lxisteps = " $lxisteps
echo "==> deltalxi = " $deltalxi
printLine

# check for existence of 'runs' directory; create it if it doesn't exist
if [ -d 'runs' ]; then
	echo "==> 'runs' directory exists"
else 
	echo "==> 'runs' directory does not exist, creating it"
	mkdir runs
fi

# check for existence of run specfic directory
id=`eval date +%Y%m%d`"_"$SLURM_JOB_ID
rundir="runs/run_"$id
if [ -d $rundir ]; then
	echo "==> run directory "$rundir" directory exists ... exiting"
	exit
else 
	echo "==> creating run directory "$rundir
	mkdir $rundir
fi

# copy necessary files and create output directories
echo "==> creating run directory structure"

printLine
echo "==> slurm job id "$SLURM_JOB_ID
echo "==> slurm node list "$SLURM_JOB_NODELIST
echo "==> slurm tasks per node "$SLURM_TASKS_PER_NODE
printLine
echo "==> Starting computation..."
printLine

# begin lxi loop 
for i in `seq 0 $lxisteps`; do

# echo `pwd`

  dir=$rundir"/"$i
  if [ -d $dir ]; then
    echo "==> run directory "$dir" directory exists already ... cleaning it out"
    rm -rf $dir
    mkdir $dir
  else
    echo "==> creating run directory "$dir
    mkdir $dir
  fi

  mkdir $dir"/output"
  cp aphoton $dir
  cp -rf input $dir
  cd $dir

  lxi=$(echo "scale=6; $lximin+$i*$deltalxi" | bc)
  xi=$(echo "scale=6; e(2.302585092994045684017991454684*($lxi))-1" | bc -l) 
  echo "==> Calculating rate for xi="$xi

  echo "==> launching job "$i
  srun -n1 --exclusive ./aphoton -XI $xi > log.txt 2> err.txt &

  # remove executable since it's no longer needed
  # rm aphoton

  cd "../../.."

done
# end lxi loop
wait # wait for jobs to finish
 
# replicate the slurm stdout file for the record and then compress it
# gzip slurm-${SLURM_JOB_ID}.out 
# cp slurm-${SLURM_JOB_ID}.out.gz $rundir/slurm.log.gz

exit
